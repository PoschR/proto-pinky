#include <iostream>

#include <SFML/Graphics.hpp>

void handle_events(sf::Window &window);

int main() {
    int const screen_width = 1024;
    int const screen_height = 768;
    sf::VideoMode const window_resolution = sf::VideoMode(screen_width, screen_height);

    sf::RenderWindow window(window_resolution, "Blinky Pinky");

    while(window.isOpen())
    {
        handle_events(window);

        window.clear(sf::Color::Black);


        window.display();
    }


    return 0;
}

void handle_events(sf::Window &window)
{
    sf::Event event;
    while(window.pollEvent(event))
    {
        if(event.type == sf::Event::Closed)
            window.close();


    }

}